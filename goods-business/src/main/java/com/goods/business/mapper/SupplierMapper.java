package com.goods.business.mapper;

import com.goods.common.model.business.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertUseGeneratedKeysMapper;

import java.util.List;

/**
 * @version v1.0
 * @ClassName SupplierMapper
 * @Description TODO
 * @Author Q
 */
@Repository
public interface SupplierMapper extends Mapper<Supplier>, InsertUseGeneratedKeysMapper<Supplier> {

    List<Supplier> findAll(@Param("name") String name, @Param("address") String address, @Param("contact") String contact);
}
