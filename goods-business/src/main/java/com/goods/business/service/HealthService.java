package com.goods.business.service;

import com.goods.common.model.health.Health;
import com.goods.common.vo.business.HealthVO;
import com.goods.common.vo.system.PageVO;

public interface HealthService {

    HealthVO isReport();

    void report(Health health);

    PageVO<HealthVO> history(Integer pageNum, Integer pageSize);
}
