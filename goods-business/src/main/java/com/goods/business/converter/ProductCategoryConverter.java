package com.goods.business.converter;

import com.goods.common.model.business.ProductCategory;
import com.goods.common.model.system.Menu;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.business.ProductCategoryVO;
import com.goods.common.vo.system.MenuNodeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName ProductCategoryConverter
 * @Description TODO
 * @Author Q
 */
public class ProductCategoryConverter {

    private static final Supplier<ProductCategoryVO> productCategoryVOSupplier = ProductCategoryVO::new;
    private static final Supplier<ProductCategoryTreeNodeVO> productCategoryTreeNodeVO = ProductCategoryTreeNodeVO::new;

    public static ProductCategoryVO converterToProductCategoryVO(ProductCategory productCategory) {
        ProductCategoryVO productCategoryVO = productCategoryVOSupplier.get();
        if (!ObjectUtils.isEmpty(productCategory)) {
            BeanUtils.copyProperties(productCategory, productCategoryVO);
        }
        return productCategoryVO;
    }

    public static List<ProductCategoryTreeNodeVO> converterToProductCategoryTreeNodeVO(List<ProductCategory> productCategoryList) {
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList = null;
        if (!CollectionUtils.isEmpty(productCategoryList)) {
            productCategoryTreeNodeVOList = productCategoryList.stream().map(productCategory -> {
                ProductCategoryTreeNodeVO productCategoryTreeNodeVO = ProductCategoryConverter.productCategoryTreeNodeVO.get();
                BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO);
                return productCategoryTreeNodeVO;
            }).collect(Collectors.toList());
        }
        return productCategoryTreeNodeVOList;
    }

}
