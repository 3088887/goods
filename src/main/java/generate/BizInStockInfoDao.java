package generate;

import generate.BizInStockInfo;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizInStockInfoDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizInStockInfo record);

    int insertSelective(BizInStockInfo record);

    BizInStockInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizInStockInfo record);

    int updateByPrimaryKey(BizInStockInfo record);
}