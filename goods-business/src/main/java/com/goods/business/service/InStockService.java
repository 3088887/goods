package com.goods.business.service;

import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version v1.0
 * @ClassName InStockService
 * @Description TODO
 * @Author Q
 */
public interface InStockService {
    PageVO<InStockVO> findInStockList(Integer pageNum, Integer pageSize, InStockVO inStockVO);

    InStockDetailVO detail(Long id);

    @Transactional
    void publish(Long id);

    void remove(Long id);

    void back(Long id);

    void delete(Long id);

    void addIntoStock(InStockVO inStockVO);
}
