package com.goods.common.model.business;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_in_stock_info
 * @author 
 */
@Table(name="biz_in_stock_info")
@ApiModel(value="generate.BizInStockInfo")
@Data
public class InStockInfo implements Serializable {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 入库单编号
     */
    @ApiModelProperty(value="入库单编号")
    private String inNum;

    /**
     * 商品编号
     */
    @ApiModelProperty(value="商品编号")
    private String pNum;

    /**
     * 数量
     */
    @ApiModelProperty(value="数量")
    private Integer productNumber;

    private Date createTime;

    private Date modifiedTime;

    private static final long serialVersionUID = 1L;
}