package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.business.OutStock;
import com.goods.common.model.business.OutStockInfo;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @version v1.0
 * @ClassName OutStockMapper
 * @Description TODO
 * @Author Q
 */
public interface OutStockInfoMapper extends Mapper<OutStockInfo>, IMapper<OutStockInfo>, MySqlMapper<OutStockInfo> {

    @Override
    default Class<?> entityClass() {
        return OutStockInfo.class;
    }

}
