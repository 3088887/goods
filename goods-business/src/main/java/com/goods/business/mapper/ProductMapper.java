package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.business.Product;
import com.goods.common.vo.business.ProductVO;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @version v1.0
 * @ClassName ProductMapper
 * @Description TODO
 * @Author Q
 */
public interface ProductMapper extends Mapper<Product>, IMapper<Product> {

    @Override
    default Class<?> entityClass() {
        return Product.class;
    }

    List<Product> findProductList(@Param("product") Product product);
}
