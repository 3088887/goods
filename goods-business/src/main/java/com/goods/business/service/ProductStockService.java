package com.goods.business.service;

import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

/**
 * @version v1.0
 * @ClassName ProductStockService
 * @Description TODO
 * @Author Q
 */
public interface ProductStockService {
    List<ProductStockVO> findAllStocks(Integer pageNum, Integer pageSize);

    PageVO<ProductStockVO> findProductStocks(Integer pageNum, Integer pageSize, String name, String categorys);
}
