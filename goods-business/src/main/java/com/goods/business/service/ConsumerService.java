package com.goods.business.service;

import com.goods.common.model.business.Consumer;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version v1.0
 * @ClassName ConsumerService
 * @Description TODO
 * @Author Q
 */
public interface ConsumerService {
    PageVO<ConsumerVO> findConsumerList(Integer pageNum, Integer pageSize, String name, String address, String contact);

    void add(Consumer consumer);

    ConsumerVO edit(Long id);

    void update(Long id, ConsumerVO consumerVO);

    @Transactional
    void delete(Long id);

    List<Consumer> findAll();
}
