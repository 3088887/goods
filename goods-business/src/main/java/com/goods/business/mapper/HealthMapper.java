package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.health.Health;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * @version v1.0
 * @ClassName HealthMapper
 * @Description TODO
 * @Author Q
 */
public interface HealthMapper extends Mapper<Health>, IMapper<Health> {

    @Override
    default Class<?> entityClass() {
        return Health.class;
    }

    @Select("SELECT id, address, user_id, situation, touch, passby, reception, create_time  FROM biz_health WHERE user_id = #{userId} AND date_format(create_time,'%Y-%m-%d') = date_format(#{date},'%Y-%m-%d');")
    Health isReport(@Param("userId") Long userId, @Param("date") String date);
}
