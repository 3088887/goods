package generate;

import generate.BizHealth;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizHealthDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizHealth record);

    int insertSelective(BizHealth record);

    BizHealth selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizHealth record);

    int updateByPrimaryKey(BizHealth record);
}