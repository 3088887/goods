package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.business.ProductCategory;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


/**
 * @version v1.0
 * @ClassName BusinessMapper
 * @Description TODO
 * @Author Q
 */
@Repository
public interface ProductCategoryMapper extends Mapper<ProductCategory>, IMapper<ProductCategory> {

    @Override
    default Class<?> entityClass() {
        return ProductCategory.class;
    }
}
