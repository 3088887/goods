package com.goods.business.converter;

import com.goods.common.model.business.Consumer;
import com.goods.common.model.health.Health;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.business.HealthVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;

/**
 * @version v1.0
 * @ClassName HealthConverter
 * @Description TODO
 * @Author Q
 */
public class HealthConverter {

    public static HealthVO converterToHealthVO(Health health) {
        if (!ObjectUtils.isEmpty(health)) {
            HealthVO healthVO = new HealthVO();
            BeanUtils.copyProperties(health, healthVO);
            return healthVO;
        }
        return null;
    }
}
