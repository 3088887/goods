package generate;

import generate.BizOutStock;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizOutStockDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizOutStock record);

    int insertSelective(BizOutStock record);

    BizOutStock selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizOutStock record);

    int updateByPrimaryKey(BizOutStock record);
}