package generate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_health
 * @author 
 */
@Table(name="biz_health")
@ApiModel(value="generate.BizHealth")
@Data
public class BizHealth implements Serializable {
    @Id
    @GeneratedValue
    @GeneratedValue(generator = "JDBC")
    private Long id;

    @NotEmpty
    private String address;

    @NotEmpty
    private Long userId;

    @NotEmpty
    private Integer situation;

    @NotEmpty
    private Integer touch;

    @NotEmpty
    private Integer passby;

    @NotEmpty
    private Integer reception;

    @NotEmpty
    private Date createTime;

    private static final long serialVersionUID = 1L;
}