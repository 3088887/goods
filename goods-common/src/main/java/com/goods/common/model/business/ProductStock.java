package com.goods.common.model.business;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_product_stock
 * @author 
 */
@Table(name="biz_product_stock")
@ApiModel(value="generate.BizProductStock")
@Data
public class ProductStock implements Serializable {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    @NotEmpty
    private String pNum;

    /**
     * 商品库存结余
     */
    @ApiModelProperty(value="商品库存结余")
    private Long stock;

    private static final long serialVersionUID = 1L;
}