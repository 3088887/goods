package generate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_in_stock
 * @author 
 */
@Table(name="biz_in_stock")
@ApiModel(value="generate.BizInStock")
@Data
public class BizInStock implements Serializable {
    @Id
    @GeneratedValue
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 入库单编号
     */
    @ApiModelProperty(value="入库单编号")
    private String inNum;

    /**
     * 类型：1：捐赠，2：下拨，3：采购,4:退货入库
     */
    @ApiModelProperty(value="类型：1：捐赠，2：下拨，3：采购,4:退货入库")
    private Integer type;

    /**
     * 操作人员
     */
    @ApiModelProperty(value="操作人员")
    private String operator;

    /**
     * 入库单创建时间
     */
    @ApiModelProperty(value="入库单创建时间")
    private Date createTime;

    /**
     * 入库单修改时间
     */
    @ApiModelProperty(value="入库单修改时间")
    private Date modified;

    /**
     * 物资总数
     */
    @ApiModelProperty(value="物资总数")
    private Integer productNumber;

    /**
     * 来源
     */
    @ApiModelProperty(value="来源")
    private Long supplierId;

    /**
     * 描述信息
     */
    @ApiModelProperty(value="描述信息")
    private String remark;

    /**
     * 0:正常入库单,1:已进入回收,2:等待审核
     */
    @ApiModelProperty(value="0:正常入库单,1:已进入回收,2:等待审核")
    private Integer status;

    private static final long serialVersionUID = 1L;
}