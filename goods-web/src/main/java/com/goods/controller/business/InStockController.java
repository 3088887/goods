package com.goods.controller.business;

import com.goods.business.service.InStockService;
import com.goods.common.model.business.InStock;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @version v1.0
 * @ClassName InStockController
 * @Description TODO
 * @Author Q
 */
@RestController
@RequestMapping("/business/inStock")
public class InStockController {

    //http://localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=0
    private final InStockService inStockService;

    public InStockController(InStockService inStockService) {
        this.inStockService = inStockService;
    }

    //http://localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=2&inNum=1&type=1&startTime=2021-09-10+00:00:00&endTime=2021-10-29+00:00:00
    @GetMapping("findInStockList")
    public ResponseBean<PageVO<InStockVO>> findInStockList(
            @RequestParam Integer pageNum,
            @RequestParam Integer pageSize,
            InStockVO inStockVO) {
        System.out.println(inStockVO);
        PageVO<InStockVO> inStockList = inStockService.findInStockList(pageNum, pageSize, inStockVO);
        return ResponseBean.success(inStockList);
    }

    @GetMapping("detail/{id}")
    public ResponseBean<InStockDetailVO> detail(@PathVariable Long id) {
        InStockDetailVO inStockDetailVO = inStockService.detail(id);
        return ResponseBean.success(inStockDetailVO);
    }


    @PutMapping("remove/{id}")
    public ResponseBean remove(@PathVariable Long id) {
        inStockService.remove(id);
        return ResponseBean.success();
    }

    @PutMapping("back/{id}")
    public ResponseBean back(@PathVariable Long id) {
        inStockService.back(id);
        return ResponseBean.success();
    }


    @PutMapping("publish/{id}")
    public ResponseBean publish(@PathVariable Long id) {
        inStockService.publish(id);
        return ResponseBean.success();
    }

    @DeleteMapping("delete/{id}")
    public ResponseBean delete(@PathVariable Long id) {
        inStockService.delete(id);
        return ResponseBean.success();
    }

    @PostMapping("addIntoStock")
    public ResponseBean addIntoStock(@RequestBody InStockVO inStockVO) {
        inStockService.addIntoStock(inStockVO);
        return ResponseBean.success();
    }
}
