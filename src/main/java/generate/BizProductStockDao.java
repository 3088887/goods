package generate;

import generate.BizProductStock;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizProductStockDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizProductStock record);

    int insertSelective(BizProductStock record);

    BizProductStock selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizProductStock record);

    int updateByPrimaryKey(BizProductStock record);
}