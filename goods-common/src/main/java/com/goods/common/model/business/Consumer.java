package com.goods.common.model.business;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_consumer
 *
 * @author
 */
@Table(name = "biz_consumer")
@Data
public class Consumer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    /**
     * 物资消费方
     */
    private String name;
    /**
     * 地址
     */
    private String address;
    private Date createTime;
    private Date modifiedTime;
    /**
     * 联系电话
     */
    private String phone;
    private Integer sort;
    /**
     * 联系人姓名
     */
    private String contact;
}