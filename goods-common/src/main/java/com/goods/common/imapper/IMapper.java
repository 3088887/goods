package com.goods.common.imapper;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

public interface IMapper<T> extends Mapper<T> {
    default List<T> selectByLambda(Tao... taos) {
        return selectByExample(make(taos));
    }

    default T selectOneByLambda(Tao... taos) {
        return selectOneByExample(make(taos));
    }

    default int selectCountByLambda(Tao... taos) {
        return selectCountByExample(make(taos));
    }

    default int deleteByLambda(Tao... taos) {
        return deleteByExample(make(taos));
    }

    default int seletCountByLambda(Tao... taos) {
        return deleteByExample(make(taos));
    }

    default int updateByLambda(T t, Tao... taos) {
        return updateByExample(t, make(taos));
    }

    default int updateSelectiveByLambda(T t, Tao... taos) {
        return updateByExampleSelective(t, make(taos));
    }

    default Example make(Tao... taos) {
        Example example = new Example(entityClass());
        for (Tao tao : taos) {
            tao.accept(example.or());
        }
        return example;
    }

    // 实际调用接口必须用default方法实现
    Class<?> entityClass();

}
