package generate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_out_stock
 * @author 
 */
@Table(name="biz_out_stock")
@ApiModel(value="generate.BizOutStock")
@Data
public class BizOutStock implements Serializable {
    @Id
    @GeneratedValue
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 出库单
     */
    @NotEmpty
    @ApiModelProperty(value="出库单")
    private String outNum;

    /**
     * 出库类型:0:直接出库,1:审核出库
     */
    @NotEmpty
    @ApiModelProperty(value="出库类型:0:直接出库,1:审核出库")
    private Integer type;

    /**
     * 操作人
     */
    @ApiModelProperty(value="操作人")
    private String operator;

    /**
     * 出库时间
     */
    @ApiModelProperty(value="出库时间")
    private Date createTime;

    /**
     * 出库总数
     */
    @ApiModelProperty(value="出库总数")
    private Integer productNumber;

    /**
     * 消费者id
     */
    @NotEmpty
    @ApiModelProperty(value="消费者id")
    private Long consumerId;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 状态:0:正常入库,1:已进入回收,2:等待审核
     */
    @ApiModelProperty(value="状态:0:正常入库,1:已进入回收,2:等待审核")
    private Integer status;

    /**
     * 紧急程度:1:不急,2:常规,3:紧急4:特急
     */
    @NotEmpty
    @ApiModelProperty(value="紧急程度:1:不急,2:常规,3:紧急4:特急")
    private Integer priority;

    private static final long serialVersionUID = 1L;
}