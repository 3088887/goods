package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.imapper.Tao;
import com.goods.common.model.business.Consumer;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertUseGeneratedKeysMapper;
import tk.mybatis.mapper.entity.Example;

/**
 * @version v1.0
 * @ClassName ConsumerMapper
 * @Description TODO
 * @Author Q
 */
public interface ConsumerMapper extends Mapper<Consumer>, IMapper<Consumer>, InsertUseGeneratedKeysMapper<Consumer> {

    @Override
    default Class<?> entityClass() {
        return Consumer.class;
    }
}
