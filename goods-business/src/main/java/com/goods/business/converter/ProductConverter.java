package com.goods.business.converter;

import com.goods.common.model.business.Product;
import com.goods.common.model.business.ProductStock;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.business.SupplierVO;

import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.function.Supplier;

/**
 * @version v1.0
 * @ClassName ProductConverter
 * @Description TODO
 * @Author Q
 */
public class ProductConverter {

    private final static Supplier<Product> productSupplier = Product::new;

    public static Product converterToProduct(ProductVO productVO) {
        Product product = productSupplier.get();
        BeanUtils.copyProperties(productVO, product);
        Long[] categoryKeys = productVO.getCategoryKeys();
        if (!ObjectUtils.isEmpty(categoryKeys)) {
            product.setOneCategoryId(categoryKeys[0]);
            product.setTwoCategoryId(categoryKeys[1]);
            product.setThreeCategoryId(categoryKeys[2]);
        }
        return product;
    }

    public static ProductStockVO converterToProductStockVO(Product product) {
        if (!ObjectUtils.isEmpty(product)) {
            ProductStockVO productStockVO = new ProductStockVO();
            BeanUtils.copyProperties(product, productStockVO);
            return productStockVO;
        }
        return null;
    }
}
