package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.ConsumerConverter;
import com.goods.business.converter.OutStockConverter;
import com.goods.business.converter.SupplierConverter;
import com.goods.business.mapper.*;
import com.goods.business.service.OutStockService;
import com.goods.common.model.business.*;
import com.goods.common.model.system.User;
import com.goods.common.response.ActiveUser;
import com.goods.common.vo.business.*;
import com.goods.common.vo.system.PageVO;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName OutStockService
 * @Description TODO
 * @Author Q
 */
@Service
public class OutStockServiceImpl implements OutStockService {

    @Autowired
    private OutStockMapper outStockMapper;

    @Autowired
    private OutStockInfoMapper outStockInfoMapper;

    @Autowired
    private ConsumerMapper consumerMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductStockMapper productStockMapper;

    @Override
    public PageVO<OutStockVO> findOutStockList(Integer pageNum, Integer pageSize, Integer status, String outNum, Integer type) {
        PageHelper.startPage(pageNum, pageSize);
        List<OutStock> outStockList = outStockMapper.selectByLambda(criteria -> {
            if (!ObjectUtils.isEmpty(status)) {
                criteria.andEqualTo("status", status);
            }
            if (!StringUtils.isEmpty(outNum)) {
                criteria.andLike("outNum", outNum + '%');
            }
            if (!ObjectUtils.isEmpty(type)) {
                criteria.andEqualTo("type", type);
            }
        });
        List<OutStockVO> outStockVOList = outStockList.stream().map(outStock -> {
            OutStockVO outStockVO = OutStockConverter.converterToOutStockVO(outStock);
            Consumer consumer = consumerMapper.selectOneByLambda(criteria ->
            {
                criteria.andEqualTo("id", outStockVO.getConsumerId());
            });
            outStockVO.setAddress(consumer.getAddress());
            outStockVO.setPhone(consumer.getPhone());
            outStockVO.setContact(consumer.getContact());
            outStockVO.setName(consumer.getName());
            return outStockVO;
        }).collect(Collectors.toList());

        PageInfo info = new PageInfo<>(outStockList);
        return new PageVO<>(info.getTotal(), outStockVOList);
    }

    @Transactional
    @Override
    public OutStockDetailVO detail(Long id) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(outStock)) {
            Consumer consumer = consumerMapper.selectByPrimaryKey(outStock.getConsumerId());
            OutStockDetailVO outStockDetailVO = new OutStockDetailVO();
            BeanUtils.copyProperties(outStock, outStockDetailVO);
            outStockDetailVO.setConsumerVO(ConsumerConverter.converterToConsumerVO(consumer));
            List<OutStockInfo> outStockInfoList = outStockInfoMapper.selectByLambda(criteria -> {
                criteria.andEqualTo("outNum", outStock.getOutNum());
            });
            AtomicLong index = new AtomicLong(1);
            List<OutStockItemVO> outStockItemVOList = outStockInfoList.stream().map(outStockInfo -> {
                Product product = productMapper.selectOneByLambda(criteria ->
                {
                    criteria.andEqualTo("pNum", outStockInfo.getPNum());
                });
                OutStockItemVO outStockItemVO = new OutStockItemVO();
                InStockItemVO inStockItemVO = new InStockItemVO();
                outStockItemVO.setModel(product.getModel());
                outStockItemVO.setImageUrl(product.getImageUrl());
                outStockItemVO.setName(product.getName());
                outStockItemVO.setPNum(product.getPNum());
                outStockItemVO.setUnit(product.getUnit());
                outStockItemVO.setCount(outStockInfo.getProductNumber());
                outStockItemVO.setId(index.getAndIncrement());
                return outStockItemVO;
            }).collect(Collectors.toList());
            outStockDetailVO.setItemVOS(outStockItemVOList);
            return outStockDetailVO;
        }
        return null;
    }


    @Transactional
    @Override
    public void addOutStock(OutStockVO outStockVO) {
        if (!ObjectUtils.isEmpty(outStockVO)) {
            if (!ObjectUtils.isEmpty(outStockVO.getConsumerId())) {
                OutStock outStock = OutStockConverter.converterToOutStock(outStockVO);
                outStock.setCreateTime(new Date());
                Integer productNumber = outStockVO.getProducts().stream().map(ProductNumberVO::getProductNumber).reduce(0, Integer::sum);
                outStock.setProductNumber(productNumber);
                outStock.setOperator(((ActiveUser) SecurityUtils.getSubject().getPrincipal()).getUsername());
                outStock.setStatus(2);
                outStock.setOutNum(DigestUtils.md5DigestAsHex(outStock.getConsumerId().toString().getBytes()));
                outStockMapper.insert(outStock);
                List<OutStockInfo> outStockInfoList = outStockVO.getProducts().stream().map(productNumberVO ->
                {
                    OutStockInfo outStockInfo = new OutStockInfo();
                    Long productId = productNumberVO.getProductId();
                    Product product = productMapper.selectByPrimaryKey(productId);
                    outStockInfo.setOutNum(outStock.getOutNum());
                    outStockInfo.setPNum(product.getPNum());
                    outStockInfo.setProductNumber(productNumberVO.getProductNumber());
                    outStockInfo.setCreateTime(new Date());
                    outStockInfo.setModifiedTime(new Date());
                    return outStockInfo;
                }).collect(Collectors.toList());
                outStockInfoMapper.insertList(outStockInfoList);
            } else {
                Consumer consumer = new Consumer();
                consumer.setAddress(outStockVO.getAddress());
                consumer.setContact(outStockVO.getContact());
                consumer.setModifiedTime(new Date());
                consumer.setContact(outStockVO.getContact());
                consumer.setName(outStockVO.getName());
                consumer.setPhone(outStockVO.getPhone());
                consumer.setSort(outStockVO.getSort());
                Long consumerId = Long.valueOf(consumerMapper.insertUseGeneratedKeys(consumer));
                outStockVO.setConsumerId(consumerId);
                addOutStock(outStockVO);
            }
        }
    }

    @Transactional
    @Override
    public void publish(Long id) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(outStock)) {
            outStock.setStatus(2);
            outStockMapper.updateByPrimaryKey(outStock);
            List<OutStockInfo> outStockInfoList = outStockInfoMapper.selectByLambda(criteria ->
            {
                criteria.andEqualTo("inNum", outStock.getOutNum());
            });
            outStockInfoList.stream().forEach(outStockInfo -> productStockMapper.updateStock(outStockInfo.getPNum(), -outStockInfo.getProductNumber()));

        }
    }

    @Transactional
    @Override
    public void remove(Long id) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(outStock)) {
            outStock.setStatus(2);
            outStockMapper.updateByPrimaryKey(outStock);
        }
    }

    @Transactional
    @Override
    public void back(Long id) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(outStock)) {
            outStock.setStatus(1);
            outStockMapper.updateByPrimaryKey(outStock);
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(outStock)) {
            outStock.setStatus(1);
            outStockMapper.updateByPrimaryKey(outStock);
        }
    }
}
