package com.goods.business.converter;

import com.goods.common.model.business.Consumer;
import com.goods.common.vo.business.ConsumerVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;

/**
 * @version v1.0
 * @ClassName ComsumerConverter
 * @Description TODO
 * @Author Q
 */
public class ConsumerConverter {

    public static ConsumerVO converterToConsumerVO(Consumer consumer) {
        if (!ObjectUtils.isEmpty(consumer)) {
            ConsumerVO consumerVO = new ConsumerVO();
            BeanUtils.copyProperties(consumer, consumerVO);
            return consumerVO;
        }
        return null;
    }

    public static Consumer converterToConsumer(ConsumerVO consumerVO) {
        if (!ObjectUtils.isEmpty(consumerVO)) {
            Consumer consumer = new Consumer();
            BeanUtils.copyProperties(consumerVO, consumer);
            return consumer;
        }
        return null;
    }
}
