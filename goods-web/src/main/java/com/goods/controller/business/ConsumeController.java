package com.goods.controller.business;

import com.goods.business.service.ConsumerService;
import com.goods.common.model.business.Consumer;
import com.goods.common.model.business.OutStock;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version v1.0
 * @ClassName ConsumeController
 * @Description TODO
 * @Author Q
 */
@RestController
@RequestMapping("/business/consumer")
public class ConsumeController {

    //business/consumer/findConsumerList?pageNum=1&pageSize=10&name=
    private final ConsumerService consumerService;

    public ConsumeController(ConsumerService consumerService) {
        this.consumerService = consumerService;
    }

    @GetMapping("findConsumerList")
    public ResponseBean<PageVO<ConsumerVO>> findConsumerList(@RequestParam Integer pageNum, @RequestParam Integer pageSize, @RequestParam(required = false) String name, @RequestParam(required = false) String address, @RequestParam(required = false) String contact) {
        PageVO<ConsumerVO> consumerList = consumerService.findConsumerList(pageNum, pageSize, name, address, contact);
        return ResponseBean.success(consumerList);
    }

    @PostMapping("add")
    public ResponseBean add(@RequestBody Consumer consumer) {
        consumerService.add(consumer);
        return ResponseBean.success();
    }

    @GetMapping("edit/{id}")
    public ResponseBean<ConsumerVO> edit(@PathVariable Long id) {
        ConsumerVO consumerV0 = consumerService.edit(id);
        return ResponseBean.success(consumerV0);
    }

    @PutMapping("update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody ConsumerVO consumerVO) {
        consumerService.update(id, consumerVO);
        return ResponseBean.success();
    }

    @DeleteMapping("delete/{id}")
    public ResponseBean delete(@PathVariable Long id) {
        consumerService.delete(id);
        return ResponseBean.success();
    }

    @GetMapping("findAll")
    public ResponseBean<List<Consumer>> findAll() {
        List<Consumer> consumerList = consumerService.findAll();
        return ResponseBean.success(consumerList);
    }
}
