package com.goods.business.service;

import com.goods.common.model.business.OutStock;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version v1.0
 * @ClassName OutStockService
 * @Description TODO
 * @Author Q
 */
public interface OutStockService {
    PageVO<OutStockVO> findOutStockList(Integer pageNum, Integer pageSize, Integer status, String outNum, Integer type);

    @Transactional
    OutStockDetailVO detail(Long id);

    void addOutStock(OutStockVO outStockVO);

    @Transactional
    void publish(Long id);

    @Transactional
    void remove(Long id);

    @Transactional
    void back(Long id);

    @Transactional
    void delete(Long id);
}
