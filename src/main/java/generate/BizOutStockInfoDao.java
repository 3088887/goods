package generate;

import generate.BizOutStockInfo;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizOutStockInfoDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizOutStockInfo record);

    int insertSelective(BizOutStockInfo record);

    BizOutStockInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizOutStockInfo record);

    int updateByPrimaryKey(BizOutStockInfo record);
}