package com.goods.business.converter;

import com.goods.common.model.business.OutStock;
import com.goods.common.vo.business.OutStockVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;

/**
 * @version v1.0
 * @ClassName OutStockConverter
 * @Description TODO
 * @Author Q
 */
public class OutStockConverter {

    public static OutStockVO converterToOutStockVO(OutStock outStock) {
        if (!ObjectUtils.isEmpty(outStock)) {
            OutStockVO outStockVO = new OutStockVO();
            BeanUtils.copyProperties(outStock, outStockVO);
            return outStockVO;
        }
        return null;
    }

    public static OutStock converterToOutStock(OutStockVO outStockVO) {
        if (!ObjectUtils.isEmpty(outStockVO)) {
            OutStock outStock = new OutStock();
            BeanUtils.copyProperties(outStockVO, outStock);
            return outStock;
        }
        return null;
    }
}
