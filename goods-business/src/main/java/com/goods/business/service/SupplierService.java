package com.goods.business.service;

import com.goods.common.model.business.Supplier;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SupplierService {
    PageVO<SupplierVO> findSupplierList(Integer pageNum, Integer pageSize, String name,String address, String contact);

    @Transactional
    void add(Supplier supplier);

    Supplier edit(Long id);

    @Transactional
    void update(Supplier supplier, Long id);

    @Transactional
    void delete(Long id);

    List<Supplier> findAll();
}
