package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.InStockConverter;
import com.goods.business.converter.SupplierConverter;
import com.goods.business.mapper.*;
import com.goods.business.service.InStockService;
import com.goods.common.model.business.InStock;
import com.goods.common.model.business.InStockInfo;
import com.goods.common.model.business.Product;
import com.goods.common.model.business.Supplier;
import com.goods.common.model.system.User;
import com.goods.common.vo.business.*;
import com.goods.common.vo.system.PageVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName InStockServiceImpl
 * @Description TODO
 * @Author Q
 */
@Service
@Slf4j
public class InStockServiceImpl implements InStockService {

    @Autowired
    private InStockMapper inStockMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private InStockInfoMapper inStockInfoMapper;

    @Autowired
    private ProductStockMapper productStockMapper;

    @Override
    public PageVO<InStockVO> findInStockList(Integer pageNum, Integer pageSize, InStockVO inStockVO) {
        PageHelper.startPage(pageNum, pageSize);

        // InStock inStock = InStockConverter.converterToInStock(inStockVO);
        List<InStock> inStockList = inStockMapper.findInStockList(inStockVO);

//        Example example = new Example(InStock.class);
//
//        Example.Criteria criteria = example.createCriteria().andEqualTo("status", inStockVO.getStatus());
//
//        if (!StringUtils.isEmpty(inStockVO.getInNum())) {
//            criteria.andEqualTo("inNum", inStockVO.getInNum());
//        }
//
//        if (!ObjectUtils.isEmpty(inStockVO.getType())) {
//            criteria.andEqualTo("type", inStockVO.getType());
//        }
//
//        if (!ObjectUtils.isEmpty(inStockVO.getStartTime()) && !ObjectUtils.isEmpty(inStockVO.getEndTime())) {
//            criteria.andBetween("createTime", inStockVO.getStartTime(), inStockVO.getEndTime());
//        }
//
//        List<InStock> inStockList = inStockMapper.selectByExample(example);

        List<InStockVO> inStockVOList = InStockConverter.converterToProductInStockVO(inStockList);
        if (!CollectionUtils.isEmpty(inStockVOList)) {
            inStockVOList.stream().forEach(inStockV0 -> {
                Long supplierId = inStockV0.getSupplierId();
                Supplier supplier = supplierMapper.selectByPrimaryKey(supplierId);
                if (!ObjectUtils.isEmpty(supplier)) {
                    inStockV0.setSupplierName(supplier.getName());
                    inStockV0.setAddress(supplier.getAddress());
                    inStockV0.setPhone(supplier.getPhone());
                }
            });


        }
        PageInfo info = new PageInfo<>(inStockList);
        return new PageVO<>(info.getTotal(), inStockVOList);
    }

    @Transactional
    @Override
    public InStockDetailVO detail(Long id) {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(inStock)) {
            Supplier supplier = supplierMapper.selectByPrimaryKey(inStock.getSupplierId());
            InStockDetailVO inStockDetailVO = new InStockDetailVO();
            BeanUtils.copyProperties(inStock, inStockDetailVO);
            inStockDetailVO.setSupplierVO(SupplierConverter.converterToSupplierVO(supplier));
            Example example = new Example(InStockInfo.class);
            example.createCriteria().andEqualTo("inNum", inStock.getInNum());
            List<InStockInfo> inStockInfos = inStockInfoMapper.selectByExample(example);
            AtomicLong index = new AtomicLong(1);
            List<InStockItemVO> inStockItemVOList = inStockInfos.stream().map(inStockInfo -> {
                Example productExample = new Example(Product.class);
                productExample.createCriteria().andEqualTo("pNum", inStockInfo.getPNum());
                Product product = productMapper.selectOneByExample(productExample);
                InStockItemVO inStockItemVO = new InStockItemVO();
                inStockItemVO.setModel(product.getModel());
                inStockItemVO.setImageUrl(product.getImageUrl());
                inStockItemVO.setName(product.getName());
                inStockItemVO.setPNum(product.getPNum());
                inStockItemVO.setUnit(product.getUnit());
                inStockItemVO.setCount(inStockInfo.getProductNumber());
                inStockItemVO.setId(index.getAndIncrement());
                return inStockItemVO;
            }).collect(Collectors.toList());
            inStockDetailVO.setItemVOS(inStockItemVOList);
            return inStockDetailVO;
        }
        return null;
    }


    @Transactional
    @Override
    public void publish(Long id) {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(inStock)) {
            inStock.setStatus(2);
            inStock.setModified(new Date());
            inStockMapper.updateByPrimaryKey(inStock);
            List<InStockInfo> inStockInfoList = inStockInfoMapper.selectByLambda(criteria ->
            {
                criteria.andEqualTo("inNum", inStock.getInNum());
            });
            inStockInfoList.stream().forEach(inStockInfo -> productStockMapper.updateStock(inStockInfo.getPNum(), inStockInfo.getProductNumber()));

        }
    }

    @Transactional
    @Override
    public void remove(Long id) {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(inStock)) {
            inStock.setStatus(2);
            inStock.setModified(new Date());
            inStockMapper.updateByPrimaryKey(inStock);
        }
    }

    @Transactional
    @Override
    public void back(Long id) {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(inStock)) {
            inStock.setStatus(1);
            inStock.setModified(new Date());
            inStockMapper.updateByPrimaryKey(inStock);
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(inStock)) {
            inStock.setStatus(1);
            inStock.setModified(new Date());
            inStockMapper.updateByPrimaryKey(inStock);
        }
    }

    @Override
    public void addIntoStock(InStockVO inStockVO) {
        if (!ObjectUtils.isEmpty(inStockVO)) {
            if (!ObjectUtils.isEmpty(inStockVO.getSupplierId())) {
                InStock inStock = InStockConverter.converterToInStock(inStockVO);
                inStock.setCreateTime(new Date());
                inStock.setModified(new Date());
                Integer productNumber = inStockVO.getProducts().stream().map(ProductNumberVO::getProductNumber).reduce(0, Integer::sum);
                inStock.setProductNumber(productNumber);
                inStock.setOperator(((User) SecurityUtils.getSubject().getPrincipal()).getUsername());
                inStock.setStatus(2);
                inStock.setInNum(DigestUtils.md5DigestAsHex(inStockVO.getSupplierId().toString().getBytes()));
                inStockMapper.insert(inStock);
                List<InStockInfo> inStockInfoList = inStockVO.getProducts().stream().map(productNumberVO -> {
                    InStockInfo inStockInfo = new InStockInfo();
                    Long productId = productNumberVO.getProductId();
                    Product product = productMapper.selectByPrimaryKey(productId);
                    inStockInfo.setInNum(inStock.getInNum());
                    inStockInfo.setPNum(product.getPNum());
                    inStockInfo.setProductNumber(productNumberVO.getProductNumber());
                    inStockInfo.setCreateTime(new Date());
                    inStockInfo.setModifiedTime(new Date());
                    return inStockInfo;
                }).collect(Collectors.toList());
                inStockInfoMapper.insertList(inStockInfoList);
                log.info("inStock: {}", inStock);
            } else {
                Supplier supplier = new Supplier();
                supplier.setAddress(inStockVO.getAddress());
                supplier.setCreateTime(new Date());
                supplier.setModifiedTime(new Date());
                supplier.setContact(inStockVO.getContact());
                supplier.setEmail(inStockVO.getEmail());
                supplier.setPhone(inStockVO.getPhone());
                supplier.setSort(inStockVO.getSort());
                supplier.setName(inStockVO.getName());
                Long supplierId = Long.valueOf(supplierMapper.insertUseGeneratedKeys(supplier));
                inStockVO.setSupplierId(supplierId);
                addIntoStock(inStockVO);
            }
        }
    }


    public int stringSize(String str) {
        int size = 0;
        for (int i = 0; i < str.length(); i++) {
            size++;
        }
        return size;
    }
}
