package com.goods.controller.business;

import com.goods.business.mapper.SupplierMapper;
import com.goods.business.service.SupplierService;
import com.goods.common.model.business.Supplier;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version v1.0
 * @ClassName GoodOraginController
 * @Description TODO
 * @Author Q
 */
@Api()
@RestController
@RequestMapping("/business/supplier")
public class SupplierController {

    private final SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    //pageNum=1&pageSize=10&name=1&address=1&contact=1

    @GetMapping("findSupplierList")
    public ResponseBean<PageVO<SupplierVO>> findSupplierList(@RequestParam Integer pageNum, @RequestParam Integer pageSize, @RequestParam String name, @RequestParam(required = false) String address, @RequestParam(required = false) String contact) {
        PageVO<SupplierVO> supplierList = supplierService.findSupplierList(pageNum, pageSize, name, address, contact);
        return ResponseBean.success(supplierList);
    }

    @PostMapping("add")
    public ResponseBean add(@RequestBody Supplier supplier) {
        supplierService.add(supplier);
        return ResponseBean.success();
    }

    @GetMapping("edit/{id}")
    public ResponseBean<Supplier> edit(@PathVariable Long id) {
        Supplier supplier = supplierService.edit(id);
        return ResponseBean.success(supplier);
    }

    @PutMapping("update/{id}")
    public ResponseBean update(@RequestBody Supplier supplier, @PathVariable Long id) {
        supplierService.update(supplier, id);
        return ResponseBean.success();
    }

    @DeleteMapping("delete/{id}")
    public ResponseBean delete(@PathVariable Long id) {
        supplierService.delete(id);
        return ResponseBean.success();
    }


    @GetMapping("findAll")
    public ResponseBean<List<Supplier>> findAll() {
        List<Supplier> supplierList = supplierService.findAll();
        return ResponseBean.success(supplierList);
    }
}
