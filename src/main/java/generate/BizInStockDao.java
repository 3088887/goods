package generate;

import generate.BizInStock;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizInStockDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizInStock record);

    int insertSelective(BizInStock record);

    BizInStock selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizInStock record);

    int updateByPrimaryKey(BizInStock record);
}