package com.goods.controller.business;

import com.goods.business.service.ProductCategoryService;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version v1.0
 * @ClassName GoodCategoryController
 * @Description TODO
 * @Author Q
 */
@Api(tags = "业务管理模块-物品类别接口")
@RestController
@RequestMapping("/business")
public class CategoryController {
    //business/productCategory/categoryTree

    @Autowired
    private ProductCategoryService productCategoryService;

    /**
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("productCategory/categoryTree")
    public ResponseBean<PageVO<ProductCategoryTreeNodeVO>> categeoryTree(@RequestParam(required = false) Integer pageNum, @RequestParam(required = false) Integer pageSize) {

        PageVO<ProductCategoryTreeNodeVO> categoryTreeList = productCategoryService.getCategoryTree(pageNum, pageSize);
        return ResponseBean.success(categoryTreeList);
    }

    @GetMapping("productCategory/getParentCategoryTree")
    public ResponseBean<List<ProductCategoryTreeNodeVO>> getParentCategoryTree() {
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList = productCategoryService.getgetParentCategoryTree();
        return ResponseBean.success(productCategoryTreeNodeVOList);
    }

    @PostMapping("productCategory/add")
    public ResponseBean add(@RequestBody ProductCategory productCategory) {
        productCategoryService.save(productCategory);
        return ResponseBean.success();
    }

    @GetMapping("productCategory/edit/{id}")
    public ResponseBean<ProductCategory> edit(@PathVariable Long id) {
        ProductCategory productCategory = productCategoryService.edit(id);
        return ResponseBean.success(productCategory);
    }

    @PutMapping("productCategory/update/{id}")
    public ResponseBean update(@RequestBody ProductCategory productCategory, @PathVariable Long id) {
        productCategoryService.update(productCategory, id);
        return ResponseBean.success();
    }

    @DeleteMapping("productCategory/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) {
        try {
            productCategoryService.delete(id);
            return ResponseBean.success();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBean.error(e.getMessage());
        }

    }

}
