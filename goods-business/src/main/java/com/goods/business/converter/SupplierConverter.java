package com.goods.business.converter;

import com.goods.common.model.business.Supplier;
import com.goods.common.vo.business.SupplierVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName SupplierConverter
 * @Description TODO
 * @Author Q
 */
public class SupplierConverter {


    public static List<SupplierVO> converterToSupplierVO(List<Supplier> supplierList) {
        if (!CollectionUtils.isEmpty(supplierList)) {
            List<SupplierVO> supplierVOList = supplierList.stream().map(supplier -> {
                SupplierVO supplierVO = new SupplierVO();
                BeanUtils.copyProperties(supplier, supplierVO);
                return supplierVO;
            }).collect(Collectors.toList());
            return supplierVOList;
        }
        return null;
    }

    public static SupplierVO converterToSupplierVO(Supplier supplier) {
        if (!ObjectUtils.isEmpty(supplier)) {
            SupplierVO supplierVO = new SupplierVO();
            BeanUtils.copyProperties(supplier, supplierVO);
            return supplierVO;
        }
        return null;
    }
}
