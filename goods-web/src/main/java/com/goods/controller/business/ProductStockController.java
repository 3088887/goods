package com.goods.controller.business;

import com.goods.business.service.ProductStockService;
import com.goods.common.model.business.ProductStock;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @version v1.0
 * @ClassName ProductStockController
 * @Description TODO
 * @Author Q
 */
@RestController
@RequestMapping("/business/product")
public class ProductStockController {

    //http://localhost:8989/business/product/findAllStocks?pageSize=9&pageNum=1
    private final ProductStockService productStockService;

    public ProductStockController(ProductStockService productStockService) {
        this.productStockService = productStockService;
    }

    @GetMapping("findAllStocks")
    public ResponseBean<List<ProductStockVO>> findAllStocks(@RequestParam(required = false) Integer pageNum, @RequestParam(required = false) Integer pageSize) {
        return ResponseBean.success(productStockService.findAllStocks(pageNum, pageSize));
    }

    //findProductStocks?pageSize=9&pageNum=1
    //pageNum=1&pageSize=6&categorys=33,34,37&name=%E8%84%91%E7%99%BD%E9%87%91
    @GetMapping("findProductStocks")
    public ResponseBean<PageVO<ProductStockVO>> findProductStocks(@RequestParam(required = false) Integer pageNum,
                                                                  @RequestParam(required = false) Integer pageSize,
                                                                  @RequestParam(required = false) String name,
                                                                  @RequestParam(required = false) String categorys) {
        PageVO<ProductStockVO> productStockPageVO = productStockService.findProductStocks(pageNum, pageSize, name, categorys);
        return ResponseBean.success(productStockPageVO);
    }


}
