package com.goods.controller.health;

import com.goods.business.service.HealthService;
import com.goods.common.model.health.Health;
import com.goods.common.response.ActiveUser;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.HealthVO;
import com.goods.common.vo.system.PageVO;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @version v1.0
 * @ClassName HealthController
 * @Description TODO
 * @Author Q
 */

@RestController
@RequestMapping("/business/health")
public class HealthController {

    private final HealthService healthService;

    public HealthController(HealthService healthService) {
        this.healthService = healthService;
    }

    @GetMapping("isReport")
    public ResponseBean<HealthVO> isReport() {
        HealthVO isReport = healthService.isReport();
        return ResponseBean.success(isReport);
    }

    @PostMapping("report")
    public ResponseBean report(@RequestBody Health health) {
        ActiveUser user = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
        healthService.report(health);
        return ResponseBean.success();
    }

    @GetMapping("history")
    public ResponseBean<PageVO<HealthVO>> history(@RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        PageVO<HealthVO> healthVOList = healthService.history(pageNum, pageSize);
        return ResponseBean.success(healthVOList);
    }
}
