package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.business.OutStockInfo;
import com.goods.common.model.business.ProductStock;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @version v1.0
 * @ClassName ProductStockMapper
 * @Description TODO
 * @Author Q
 */
public interface ProductStockMapper extends Mapper<ProductStock>, IMapper<ProductStock> {

    @Override
    default Class<?> entityClass()
    {
        return ProductStock.class;
    }

    @Update("UPDATE biz_product_stock SET stock = stock + #{productNumber} WHERE p_num = #{pNum};")
    void updateStock(@Param("pNum") String pNum,@Param("productNumber") Integer productNumber);
    
}
