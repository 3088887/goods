package com.goods.controller.business;

import com.goods.business.service.ProductService;
import com.goods.common.model.business.Product;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @version v1.0
 * @ClassName ProductController
 * @Description TODO
 * @Author Q
 */
@RestController
@RequestMapping("/business/product")
public class ProductController {

    //http://localhost:8989/business/product/findProductList?pageNum=1&pageSize=6&name=&categoryId=&supplier=&status=0
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("findProductList")
    public ResponseBean<PageVO<Product>> findProductList(
            @RequestParam Integer pageNum,
            @RequestParam Integer pageSize,
            @RequestParam String name,
            @RequestParam(required = false) String categorys,
            @RequestParam Integer status
    ) {

        PageVO<Product> productList = productService.findProductList(pageNum, pageSize, name, categorys, status);
        return ResponseBean.success(productList);
    }

    @PostMapping("add")
    public ResponseBean add(@RequestBody ProductVO productVO) {
        productService.save(productVO);
        return ResponseBean.success();
    }

    @GetMapping("edit/{id}")
    public ResponseBean<Product> edit(@PathVariable Long id) {
        Product product = productService.edit(id);
        return ResponseBean.success(product);
    }

    @PutMapping("update/{id}")
    public ResponseBean<Product> update(@PathVariable Long id, ProductVO productVO) {
        productService.update(id, productVO);
        return ResponseBean.success();
    }

    @PutMapping("remove/{id}")
    public ResponseBean remove(@PathVariable Long id) {
        productService.remove(id);
        return ResponseBean.success();
    }

    @PutMapping("back/{id}")
    public ResponseBean back(@PathVariable Long id) {
        productService.back(id);
        return ResponseBean.success();
    }


    @PutMapping("publish/{id}")
    public ResponseBean publish(@PathVariable Long id) {
        productService.publish(id);
        return ResponseBean.success();
    }

    @DeleteMapping("delete/{id}")
    public ResponseBean delete(@PathVariable Long id) {
        productService.delete(id);
        return ResponseBean.success();
    }


    @GetMapping("findProducts")
    public ResponseBean<PageVO<Product>> findProducts(@RequestParam Integer pageNum, @RequestParam Integer pageSize,
                                                      @RequestParam Integer status, @RequestParam(required = false) String name,
                                                      @RequestParam(required = false) String categorys) {
        PageVO<Product> products = productService.findProducts(pageNum, pageSize, name, categorys, status);
        return ResponseBean.success(products);
    }



}
