package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.business.OutStock;
import com.goods.common.vo.business.OutStockVO;
import tk.mybatis.mapper.common.Mapper;

/**
 * @version v1.0
 * @ClassName OutStockMapper
 * @Description TODO
 * @Author Q
 */
public interface OutStockMapper extends Mapper<OutStock>, IMapper<OutStock> {

    @Override
    default Class<?> entityClass() {
        return OutStock.class;
    }
}
