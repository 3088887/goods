package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.ConsumerConverter;
import com.goods.business.mapper.ConsumerMapper;
import com.goods.business.service.ConsumerService;
import com.goods.common.model.business.Consumer;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName ConsumerServiceImpl
 * @Description TODO
 * @Author Q
 */
@Service
public class ConsumerServiceImpl implements ConsumerService {

    @Autowired
    private ConsumerMapper consumerMapper;

    @Override
    public PageVO<ConsumerVO> findConsumerList(Integer pageNum, Integer pageSize, String name, String address, String contact) {
        PageHelper.startPage(pageNum, pageSize);
        List<Consumer> consumerList = consumerMapper.selectByLambda(consumer -> {
            if (!StringUtils.isEmpty(name)) {
                consumer.andLike("name", '%' + name + '%');
            }
            if (!StringUtils.isEmpty(address)) {
                consumer.andLike("address", '%' + address + '%');
            }
            if (!StringUtils.isEmpty(contact)) {
                consumer.andEqualTo("contact", contact);
            }
        });
        List<ConsumerVO> consumerVOList = consumerList.stream().map(consumer -> ConsumerConverter.converterToConsumerVO(consumer)).collect(Collectors.toList());
        PageInfo info = new PageInfo<>(consumerList);
        return new PageVO<>(info.getTotal(), consumerVOList);
    }

    @Transactional
    @Override
    public void add(Consumer consumer) {
        consumer.setCreateTime(new Date());
        consumer.setModifiedTime(new Date());
        consumerMapper.insert(consumer);
    }

    @Override
    public ConsumerVO edit(Long id) {
        Consumer consumer = consumerMapper.selectByPrimaryKey(id);
        return ConsumerConverter.converterToConsumerVO(consumer);
    }

    @Transactional
    @Override
    public void update(Long id, ConsumerVO consumerVO) {
        Consumer consumer = ConsumerConverter.converterToConsumer(consumerVO);
        consumer.setModifiedTime(new Date());
        consumer.setId(id);
        consumerMapper.updateByPrimaryKey(consumer);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        consumerMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Consumer> findAll() {
        return consumerMapper.selectAll();
    }
}
