package com.goods.business.mapper;

import com.goods.common.imapper.IMapper;
import com.goods.common.model.business.InStock;
import com.goods.common.model.business.InStockInfo;
import com.goods.common.vo.business.InStockVO;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

/**
 * @version v1.0
 * @ClassName InStockInfoMapper
 * @Description TODO
 * @Author Q
 */
public interface InStockInfoMapper extends Mapper<InStockInfo>, MySqlMapper<InStockInfo>, IMapper<InStockInfo> {

    @Override
    default Class<?> entityClass() {
        return InStockInfo.class;
    }
}
