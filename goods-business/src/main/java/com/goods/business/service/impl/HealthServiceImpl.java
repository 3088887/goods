package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.HealthConverter;
import com.goods.business.mapper.HealthMapper;
import com.goods.business.service.HealthService;
import com.goods.common.model.health.Health;
import com.goods.common.model.system.User;
import com.goods.common.response.ActiveUser;
import com.goods.common.vo.business.HealthVO;
import com.goods.common.vo.system.PageVO;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HealthServiceImpl implements HealthService {

    @Autowired
    private HealthMapper healthMapper;

    @Override
    public HealthVO isReport() {
        User user = getUser();
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        Health health = healthMapper.isReport(user.getId(), simpleDateFormat.format(now));
        return HealthConverter.converterToHealthVO(health);
    }

    @Override
    public void report(Health health) {
        User user = getUser();
        health.setUserId(user.getId());
        health.setCreateTime(new Date());
        healthMapper.insert(health);
    }

    @Override
    public PageVO<HealthVO> history(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        User user = getUser();
        List<Health> healthList = healthMapper.selectByLambda(criteria -> {
            criteria.andEqualTo("userId", user.getId());
        });
        List<HealthVO> healthVOList = healthList.stream().map(health -> HealthConverter.converterToHealthVO(health)).collect(Collectors.toList());
        PageInfo info = new PageInfo<>(healthList);
        return new PageVO<>(info.getTotal(), healthVOList);
    }

    private User getUser() {
        ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
        return activeUser.getUser();
    }
}
