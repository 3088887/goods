package com.goods.controller.business;

import com.goods.business.service.OutStockService;
import com.goods.common.model.business.OutStock;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.web.bind.annotation.*;

/**
 * @version v1.0
 * @ClassName OutStockController
 * @Description TODO
 * @Author Q
 */
@RestController
@RequestMapping("/business/outStock")
public class OutStockController {

    //http://localhost:8989/business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0

    private final OutStockService outStockService;

    public OutStockController(OutStockService outStockService) {
        this.outStockService = outStockService;
    }

    @GetMapping("findOutStockList")
    public ResponseBean<PageVO<OutStockVO>> findOutStockList(
            @RequestParam Integer pageNum,
            @RequestParam Integer pageSize,
            @RequestParam Integer status,
            @RequestParam(required = false) String outNum,
            @RequestParam(required = false) Integer type
    ) {
        PageVO<OutStockVO> outStockList = outStockService.findOutStockList(pageNum, pageSize, status, outNum, type);
        return ResponseBean.success(outStockList);
    }

    @GetMapping("detail/{id}")
    public ResponseBean<OutStockDetailVO> detail(@PathVariable Long id) {
        OutStockDetailVO outStockDetailVO = outStockService.detail(id);
        return ResponseBean.success(outStockDetailVO);
    }

    @PostMapping("addOutStock")
    public ResponseBean addOutStock(@RequestBody OutStockVO outStockVO) {
        outStockService.addOutStock(outStockVO);
        return ResponseBean.success();
    }


    @PutMapping("remove/{id}")
    public ResponseBean remove(@PathVariable Long id) {
        outStockService.remove(id);
        return ResponseBean.success();
    }

    @PutMapping("back/{id}")
    public ResponseBean back(@PathVariable Long id) {
        outStockService.back(id);
        return ResponseBean.success();
    }


    @PutMapping("publish/{id}")
    public ResponseBean publish(@PathVariable Long id) {
        outStockService.publish(id);
        return ResponseBean.success();
    }

    @DeleteMapping("delete/{id}")
    public ResponseBean delete(@PathVariable Long id) {
        outStockService.delete(id);
        return ResponseBean.success();
    }


}
