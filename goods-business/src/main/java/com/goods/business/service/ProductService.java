package com.goods.business.service;

import com.goods.common.model.business.Product;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version v1.0
 * @ClassName ProductService
 * @Description TODO
 * @Author Q
 */
public interface ProductService {


    PageVO<Product> findProductList(Integer pageNum, Integer pageSize, String name, String categoryId, Integer status);

    void save(ProductVO productVO);

    Product edit(Long id);

    @Transactional
    void update(Long id, ProductVO productVO);

    @Transactional
    void remove(Long id);

    @Transactional
    void back(Long id);

    void publish(Long id);

    void delete(Long id);

    PageVO<Product> findProducts(Integer pageNum, Integer pageSize, String name, String categorys, Integer status);
}
