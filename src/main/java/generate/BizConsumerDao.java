package generate;

import generate.BizConsumer;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

public interface BizConsumerDao {
    int deleteByPrimaryKey(Long id);

    int insert(BizConsumer record);

    int insertSelective(BizConsumer record);

    BizConsumer selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizConsumer record);

    int updateByPrimaryKey(BizConsumer record);
}