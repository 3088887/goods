package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.ProductConverter;
import com.goods.business.mapper.ProductMapper;
import com.goods.business.service.ProductService;
import com.goods.common.model.business.Product;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @version v1.0
 * @ClassName ProductServiceImpl
 * @Description TODO
 * @Author Q
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public PageVO<Product> findProductList(Integer pageNum, Integer pageSize, String name, String categorys, Integer status) {
        PageHelper.startPage(pageNum, pageSize);
        Product product = new Product();
        product.setName(name);
        product.setStatus(status);
        if (!ObjectUtils.isEmpty(categorys)) {
            String[] categoryIds = categorys.split(",");
            if (categoryIds.length > 0) {
                product.setOneCategoryId(Long.parseLong(categoryIds[0]));
            }
            if (categoryIds.length > 1) {
                product.setTwoCategoryId(Long.parseLong(categoryIds[1]));
            }
            if (categoryIds.length > 2) {
                product.setThreeCategoryId(Long.parseLong(categoryIds[2]));
            }
        }
        List<Product> productList = productMapper.findProductList(product);
        PageInfo info = new PageInfo(productList);
        return new PageVO<>(info.getTotal(), productList);
    }

    @Transactional
    @Override
    public void save(ProductVO productVO) {
        Product product = ProductConverter.converterToProduct(productVO);
        product.setStatus(2);
        product.setCreateTime(new Date());
        product.setModifiedTime(new Date());
        productMapper.insert(product);
    }

    @Override
    public Product edit(Long id) {
        return productMapper.selectByPrimaryKey(id);
    }

    @Transactional
    @Override
    public void update(Long id, ProductVO productV0) {
        if (productV0 != null) {
            Product product = ProductConverter.converterToProduct(productV0);
            Example example = new Example(Product.class);
            example.createCriteria().andEqualTo("id", id);
            product.setModifiedTime(new Date());
            productMapper.updateByExample(product, example);
        }

    }

    @Transactional
    @Override
    public void remove(Long id) {
        Product product = productMapper.selectByPrimaryKey(id);
        product.setStatus(1);
        product.setModifiedTime(new Date());
        productMapper.updateByPrimaryKey(product);
    }

    @Transactional
    @Override
    public void back(Long id) {
        Product product = productMapper.selectByPrimaryKey(id);
        product.setStatus(0);
        product.setModifiedTime(new Date());
        productMapper.updateByPrimaryKey(product);
    }

    @Transactional
    @Override
    public void publish(Long id) {
        Product product = productMapper.selectByPrimaryKey(id);
        product.setStatus(0);
        product.setModifiedTime(new Date());
        productMapper.updateByPrimaryKey(product);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        productMapper.deleteByPrimaryKey(id);
    }

    @Override
    public PageVO<Product> findProducts(Integer pageNum, Integer pageSize, String name, String categorys, Integer status) {
        return findProductList(pageNum, pageSize, name, categorys, status);
    }

}
