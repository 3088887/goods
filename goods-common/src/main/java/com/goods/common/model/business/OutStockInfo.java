package com.goods.common.model.business;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * biz_out_stock_info
 *
 * @author
 */
@Table(name = "biz_out_stock_info")
@ApiModel(value = "generate.BizOutStockInfo")
@Data
public class OutStockInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    private String outNum;
    private String pNum;
    private Integer productNumber;
    private Date createTime;
    private Date modifiedTime;
}