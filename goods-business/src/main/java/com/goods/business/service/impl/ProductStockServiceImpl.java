package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.ProductConverter;
import com.goods.business.mapper.ProductMapper;
import com.goods.business.mapper.ProductStockMapper;
import com.goods.business.service.ProductStockService;
import com.goods.common.model.business.Product;
import com.goods.common.model.business.ProductStock;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName ProductStockServiceImpl
 * @Description TODO
 * @Author Q
 */
@Service
public class ProductStockServiceImpl implements ProductStockService {

    private final Map<String, Object> cache = new ConcurrentHashMap<>();
    @Autowired
    private ProductStockMapper productStockMapper;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<ProductStockVO> findAllStocks(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return (List<ProductStockVO>) cache.get("productStockVOList");
    }

    @Override
    public PageVO<ProductStockVO> findProductStocks(Integer pageNum, Integer pageSize, String name, String categorys) {
        PageHelper.startPage(pageNum, pageSize);
        List<Product> productList = productMapper.selectByLambda(criteria -> {
            criteria.andEqualTo("status", 0);
            if (!StringUtils.isEmpty(name)) {
                criteria.andLike("name", '%' + name + '%');
            }
            if (!StringUtils.isEmpty(categorys)) {
                String[] categoryArray = categorys.split(",");
                if (categoryArray.length > 0) {
                    criteria.andEqualTo("oneCategoryId", categoryArray[0]);
                }
                if (categoryArray.length > 1) {
                    criteria.andEqualTo("twoCategoryId", categoryArray[1]);
                }
                if (categoryArray.length > 2) {
                    criteria.andEqualTo("threeCategoryId", categoryArray[2]);
                }
            }
        });
        List<ProductStockVO> productStockVOList = productList.stream().map(product -> {
            ProductStockVO productStockVO = ProductConverter.converterToProductStockVO(product);
            Example example = new Example(ProductStock.class);
            example.createCriteria().andEqualTo("pNum", product.getPNum());
            ProductStock productStock = productStockMapper.selectOneByExample(example);
            if (!ObjectUtils.isEmpty(productStock)) {
                productStockVO.setStock(productStock.getStock());
            }

            return productStockVO;
        }).collect(Collectors.toList());
        //存缓存
        cache.put("productStockVOList", productStockVOList);
        PageInfo info = new PageInfo(productStockVOList);
        return new PageVO<>(info.getTotal(), productStockVOList);
    }
}
