package com.goods.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.SupplierConverter;
import com.goods.business.mapper.SupplierMapper;
import com.goods.business.service.SupplierService;
import com.goods.common.model.business.Supplier;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @version v1.0
 * @ClassName SupplierServiceImpl
 * @Description TODO
 * @Author Q
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    private final SupplierMapper supplierMapper;

    public SupplierServiceImpl(SupplierMapper supplierMapper) {
        this.supplierMapper = supplierMapper;
    }


    @Override
    public PageVO<SupplierVO> findSupplierList(Integer pageNum, Integer pageSize, String name, String address, String contact) {
        PageHelper.startPage(pageNum, pageSize);

        List<Supplier> supplierList = supplierMapper.findAll(name, address, contact);
        Example example = new Example(Supplier.class);

        //List<Supplier> supplierList = supplierMapper.selectAll();
        List<SupplierVO> supplierVOList = SupplierConverter.converterToSupplierVO(supplierList);
        PageInfo info = new PageInfo(supplierList);
        return new PageVO<>(info.getTotal(), supplierVOList);
    }

    @Transactional
    @Override
    public void add(Supplier supplier) {
        supplier.setCreateTime(new Date());
        supplier.setModifiedTime(new Date());
        supplierMapper.insert(supplier);
    }

    @Override
    public Supplier edit(Long id) {
        return supplierMapper.selectByPrimaryKey(id);
    }

    @Transactional
    @Override
    public void update(Supplier supplier, Long id) {
        supplier.setModifiedTime(new Date());
        Example example = new Example(Supplier.class);
        example.createCriteria().andEqualTo("id", id);
        supplierMapper.updateByExample(supplier, example);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        supplierMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Supplier> findAll() {
        return supplierMapper.selectAll();
    }
}
