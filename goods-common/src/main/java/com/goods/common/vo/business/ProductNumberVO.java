package com.goods.common.vo.business;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName ProductNumberVO
 * @Description TODO
 * @Author Q
 */
@Data
public class ProductNumberVO {

    private Long productId;

    private Integer productNumber;
}
