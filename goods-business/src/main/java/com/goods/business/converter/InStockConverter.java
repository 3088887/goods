package com.goods.business.converter;

import com.goods.common.model.business.InStock;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName InStockConverter
 * @Description TODO
 * @Author Q
 */
public class InStockConverter {
    public static List<InStockVO> converterToProductInStockVO(List<InStock> inStockList) {
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList = null;
        if (!CollectionUtils.isEmpty(inStockList)) {
            List<InStockVO> inStockVOList = inStockList.stream().map(inStock -> {
                InStockVO inStockVO = new InStockVO();
                BeanUtils.copyProperties(inStock, inStockVO);
                return inStockVO;
            }).collect(Collectors.toList());
            return inStockVOList;
        }
        return null;
    }

    public static InStock converterToInStock(InStockVO inStockVO) {
        if (!ObjectUtils.isEmpty(inStockVO)) {
            InStock inStock = new InStock();
            BeanUtils.copyProperties(inStockVO, inStock);
            return inStock;
        }
        return null;
    }
}
