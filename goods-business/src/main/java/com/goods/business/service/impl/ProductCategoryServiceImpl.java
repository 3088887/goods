package com.goods.business.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.ProductCategoryConverter;
import com.goods.business.mapper.ProductCategoryMapper;
import com.goods.business.service.ProductCategoryService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.utils.CategoryTreeBuilder;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Override
    public PageVO<ProductCategoryTreeNodeVO> getCategoryTree(Integer pageNum, Integer pageSize) {
        //查出非一级数据
        List<ProductCategory> productChildrenCategoryList = productCategoryMapper.selectByLambda(criteria -> criteria.andNotEqualTo("pid", 0));
        //有分页分页，无分页则不分
        if (!ObjectUtils.isEmpty(pageNum) && !ObjectUtils.isEmpty(pageSize)) {
            PageHelper.startPage(pageNum, pageSize);
        }
        //查询一级分类并进行分页
        List<ProductCategory> productParentCategoryList = productCategoryMapper.selectByLambda(criteria -> criteria.andEqualTo("pid", 0));
        //productCategories.
        //合并一级和非一级数据
        List<ProductCategory> productCategoryList = Stream.of(productParentCategoryList, productChildrenCategoryList).flatMap(Collection::stream).collect(Collectors.toList());
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList = ProductCategoryConverter.converterToProductCategoryTreeNodeVO(productCategoryList);
        List<ProductCategoryTreeNodeVO> categoryTreeNodeVOList = CategoryTreeBuilder.build(productCategoryTreeNodeVOList);

        PageInfo info = new PageInfo(productParentCategoryList);
        //long total = productCategoryList.stream().filter(productCategory -> productCategory.getPid() == 0).count();
        return new PageVO<ProductCategoryTreeNodeVO>(info.getTotal(), categoryTreeNodeVOList);
    }

    @Override
    public List<ProductCategoryTreeNodeVO> getgetParentCategoryTree() {
        List<ProductCategory> productCategoryList = productCategoryMapper.selectAll();
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList = ProductCategoryConverter.converterToProductCategoryTreeNodeVO(productCategoryList);
        return CategoryTreeBuilder.buildParent(productCategoryTreeNodeVOList);

    }

    @Transactional
    @Override
    public void save(ProductCategory productCategory) {
        productCategory.setCreateTime(new Date());
        productCategory.setModifiedTime(new Date());
        productCategoryMapper.insert(productCategory);
    }

    @Override
    public ProductCategory edit(Long id) {
        return productCategoryMapper.selectByPrimaryKey(id);
    }

    @Transactional
    @Override
    public void update(ProductCategory productCategory, Long id) {
        Example example = new Example(ProductCategory.class);
        example.createCriteria().andEqualTo("id", id);
        productCategory.setModifiedTime(new Date());
        productCategoryMapper.updateByExample(productCategory, example);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Example example = new Example(ProductCategory.class);
        example.createCriteria().andEqualTo("pid", id);
        int count = productCategoryMapper.selectCountByExample(example);
        if (count > 0) {
            throw new RuntimeException("存在子分类，无法删除");
        }
        productCategoryMapper.deleteByPrimaryKey(id);
    }
}
