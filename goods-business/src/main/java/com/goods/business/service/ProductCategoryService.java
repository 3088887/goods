package com.goods.business.service;

import com.goods.common.model.business.ProductCategory;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

public interface ProductCategoryService {

    PageVO<ProductCategoryTreeNodeVO> getCategoryTree(Integer pageNum, Integer pageSize);

    List<ProductCategoryTreeNodeVO> getgetParentCategoryTree();

    void save(ProductCategory productCategory);

    ProductCategory edit(Long id);

    void update(ProductCategory productCategory, Long id);

    void delete(Long id);
}
